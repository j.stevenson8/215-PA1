#include <stdio.h>
#include <string.h>
#include <ctype.h>

void get_string(char *);
void check_elements(char *);
void string_copy(char *, char *);
void print_string(char *);
int cardinality(char *);
void string_union(char *, char *, char *);
void string_intersection(char *, char *, char *);
void string_difference(char *, char *, char *);

int main(void)
{
	char A[100], B[100];
	char union_string[200];
	char intersection_string[100];
	char difference_string[100];

	get_string(A);
	get_string(B);

	check_elements(A);
	check_elements(B);

	printf("A:"); print_string(A);
	printf("B:"); print_string(B);	

	printf("|A|: %d\n", cardinality(A));
	printf("|B|: %d\n", cardinality(B));

	string_union(A,B,union_string);
	printf("Union:"); print_string(union_string);

	string_intersection(A,B,intersection_string);
	printf("Intersection:"); print_string(intersection_string);

	string_difference(A,B,difference_string);
	printf("Difference:"); print_string(difference_string);

	return 0;

}

void get_string(char *string)
{
	int i=0;

	printf("Please enter a string: ");
	fgets(string, 100, stdin);
	
	string[strlen(string)-1] = '\0';
}

void check_elements(char *string)
{
	int i=0, j=0;
	int count=0;
	char out[100];
	int is_element=0;

	out[0]='\0';

	while (i < strlen(string))
	{
		while ((is_element == 0) && (j < strlen(out)))
		{
			if (string[i] == out[j])
			{
				is_element = 1;
			}
	
			j++;
		}
		
		if (is_element == 0)
		{
			if (isspace(string[i])==0)
			{
				out[count] = string[i];
				count++;
				out[count] = '\0';
			}
		}
	
		j=0;
		i++;
		is_element=0;
	}

	string_copy(out, string);
	
	return;
}

void string_copy(char *input,char *output)
{
	int i=0;

	while (i < strlen(input))
	{
		output[i] = input[i];
		i++;
	}
	
	output[i]='\0';

	return;
}

void print_string(char *string)
{
	int i=1;
	
	if(strlen(string) == 0)	
	{
		printf("{}\n");
		return;
	}
	
	printf("{%c", string[0]);

	while (i < strlen(string))
	{
		printf(", %c", string[i]);
		i++;
	}
	
	printf("}\n");
	
	return;
}

int cardinality(char *string)
{
	return strlen(string);
}

void string_union(char *string_A, char *string_B, char *out)
{
	int i=0, j=0;
	int count=0;
	int is_element=0;

	string_copy(string_A, out);
	count=strlen(out);

	while (i < strlen(string_B))
	{
		while ((is_element == 0) && (j < strlen(out)))
		{
			if (string_B[i] == out[j])
			{
				is_element = 1;
			}
	
			j++;
		}
		
		if (is_element == 0)
		{
			out[count] = string_B[i];
			count++;
			out[count] = '\0';
		}
	
		j=0;
		i++;
		is_element=0;
	}
	return;
}


void string_intersection(char *string_A, char *string_B, char *out)
{
	int i=0, j=0;
	int count=0;
	int is_element=0;

	out[0]='\0';

	while (i < strlen(string_B))
	{
		while ((is_element == 0) && (j < strlen(string_A)))
		{
			if (string_B[i] == string_A[j])
			{
				is_element = 1;
			}
	
			j++;
		}
		
		if (is_element == 1)
		{
			out[count] = string_B[i];
			count++;
			out[count] = '\0';
		}
	
		j=0;
		i++;
		is_element=0;
	}
	return;
}


void string_difference(char *string_A, char *string_B, char *out)
{
	int i=0, j=0;
	int count=0;
	int is_element=0;
	
	out[0]='\0';

	while (i < strlen(string_A))
	{
		while ((is_element == 0) && (j < strlen(string_B)))
		{
			if (string_A[i] == string_B[j])
			{
				is_element = 1;
			}
	
			j++;
		}
		
		if (is_element == 0)
		{
			out[count] = string_A[i];
			count++;
			out[count] = '\0';
		}
	
		j=0;
		i++;
		is_element=0;
	}
	return;
}


